/*
 * websockify-wrapper.c
 *
 * Simple SGID wrapper around websockify to allow websockify to read
 * the web server TLS certificate and private key.
 *
 * This is for systems which do not allow SGID scripts.
 *
 */

#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>

int main( int argc, char **argv, char **envp ) {

    char **newargv;
    int i;

    newargv = (char **) malloc( (argc + 4) * sizeof(char *) );
    if ( newargv == NULL ) {
        fprintf( stderr, "ERROR: unable to allocate memory\n" );
        return 1;
    }

    newargv[0] = "/usr/bin/python";
    newargv[1] = "-E";  /* security: ignore PYTHONPATH, PYTHONHOME */
    newargv[2] = "-s";  /* security: don't add user site dir to sys.path */
    newargv[3] = "/usr/bin/websockify";  /* script to run */

    /* copy all arguments except for the program name argv[0] */
    for ( i=1 ; i < argc ; i++ ) {
        newargv[3+i] = argv[i];
    }
    newargv[3+argc] = NULL;

#if 0
    /* for debugging */
    for (i=0; i<argc+3; i++) {
        printf( "argument %d: %s\n", i, newargv[i] );
    }
#endif

    execve( "/usr/bin/python", newargv, envp );

    /* If we get here, something went wrong */
    perror( "ERROR: unable to start /usr/bin/websockify" );
    return 1;

}

