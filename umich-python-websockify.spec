Name:           umich-python-websockify
Version:        0.7.0
Release:        2%{?dist}
Summary:        WSGI based adapter for the Websockets protocol

License:        LGPLv3
URL:            https://github.com/kanaka/websockify
Source0:        websockify-%{version}.tar.gz
Source1:        websockify-wrapper.c
Source2:        vis-dev.arc-ts.umich.edu.pem
Patch0:         websockify-0.7.0-sgidwrapper.patch
BuildRequires:  python2-devel
BuildRequires:  python-setuptools

Requires:       python-setuptools
Requires(pre):	shadow-utils
Conflicts:	python-websockify

%description
Python WSGI based adapter for the Websockets protocol

%prep
%setup -q -n websockify-%{version}
%patch0 -p1 -b .suidwrapper

# TODO: Have the following handle multi line entries
sed -i '/setup_requires/d; /install_requires/d; /dependency_links/d' setup.py

%build
%{__python} setup.py build
gcc -O3 -g -Wall -o websockify-wrapper %{SOURCE1}


%install
%{__python} setup.py install -O1 --skip-build --root %{buildroot}
install websockify-wrapper %{buildroot}%{_bindir}/
mkdir -p %{buildroot}%{_sysconfdir}/pki
mkdir -m 750 -p %{buildroot}%{_sysconfdir}/pki/websockify
install -m 640 %{SOURCE2} %{buildroot}%{_sysconfdir}/pki/websockify/

rm -Rf %{buildroot}/usr/share/websockify
mkdir -p %{buildroot}%{_mandir}/man1/
install -m 444 docs/websockify.1 %{buildroot}%{_mandir}/man1/

%pre
getent group websockify >/dev/null || groupadd -r websockify


%files
%doc LICENSE.txt docs
%{_mandir}/man1/websockify.1*
%{python_sitelib}/websockify/*
%{python_sitelib}/websockify-%{version}-py?.?.egg-info
%{_bindir}/websockify
%attr(2755,root,websockify) %{_bindir}/websockify-wrapper
%attr(0750,root,websockify) %{_sysconfdir}/pki/websockify
%attr(0640,root,websockify) %{_sysconfdir}/pki/websockify/*

%changelog
* Sun Dec 13 2015 Mark Montagye <markmont@umich.edu>
- 0.7.0-2 development version (0.7.0 release plus recent commits)
- Added websockify-0.7.0-sgidwrapper.patch to allow daemon to continue to
  access the certificate protected by the group used by the SGID wrapper.
* Fri Dec 11 2015 Mark Montagye <markmont@umich.edu>
- 0.7.0-1 development version (0.7.0 release plus recent commits)
- rename to umich-python-websockify
- Add wrapper to restrict access to certificate
- Add U-M development certificate
* Thu Jun 18 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.6.0-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_23_Mass_Rebuild

* Wed Apr 29 2015 Pádraig Brady <pbrady@redhat.com> - 0.6.0-2
- Support big endian systems - rhbz#1216219

* Mon Mar 23 2015 Nikola Đipanov <ndipanov@redhat.com> - 0.6.0-1
- Update to release 0.6.0

* Sun Jun 08 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.5.1-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_Mass_Rebuild

* Thu Sep 10 2013 Nikola Đipanov <ndipanov@redhat.com> - 0.5.1-1
- Update to release 0.5.1

* Sun Aug 04 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.4.1-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_20_Mass_Rebuild

* Thu Jun 20 2013 Pádraig Brady <P@draigBrady.com> - 0.4.1-1
- Update to release 0.4.1

* Tue Mar 12 2013 Pádraig Brady <P@draigBrady.com> - 0.2.0-4
- Add runtime dependency on setuptools

* Fri Nov 2 2012 Nikola Đipanov <ndipanov@redhat.com> - 0.2.0-1
- Moving to the upstream version 0.2.0

* Wed Oct 31 2012 Pádraig Brady <P@draigBrady.com> - 0.1.0-6
- Remove hard dependency on numpy

* Thu Jun 14 2012 Pádraig Brady <P@draigBrady.com> - 0.1.0-5
- Removed hard dependency on numpy

* Wed Jun 6 2012 Adam Young <ayoung@redhat.com> - 0.1.0-4
- Added Description
- Added Manpage

* Fri May 11 2012 Matthias Runge <mrunge@matthias-runge.de> - 0.1.0-2
- spec cleanup

* Thu May 10 2012 Adam Young <ayoung@redhat.com> - 0.1.0-1
- Initial RPM release.
