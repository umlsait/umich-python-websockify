
# umich-python-websockify

These are the files needed to build a CentOS 7 RPM for a modified version of [websockify](https://github.com/kanaka/websockify).

The modifications to websockify allow it to be run as a normal user without giving users access to the TLS certificate private key.  The special version of websockify includes:

* A tar archive of websockify's git repo contents as of December 11, 2015 (version 0.7.0 plus unreleased commits).
* A patch to websockify that avoids dropping setgid privileges when running as a daemon.
* A wrapper program that is installed setgid to allow websockify to be invoked with special privileges on systems that do not permit setgid scripts.
* A sample certificate chain (`vis-dev.arc-ts.umich.edu.pem`) with a placeholder at the end of the file instead of the actual private key.

The RPM is based on the RPM spec file [from EPEL 6](https://dl.fedoraproject.org/pub/epel/6/SRPMS/repoview/python-websockify.html).  The RPM will

* Install websockify under `/usr`.
* Create a new group named "websockify".
* Install the wrapper program as `/usr/bin/websockify-wrapper` setgid to group "websockify".
* Install the certificate under /etc/pki/websockify such that it is only readable by processes with group "websockify".

## Installation

1. Place all files in your `rpmbuild/SOURCES` directory, except for `umich-python-websockify.spec` which should go under `rpmbuild/SPECS`.
1. Replace `SOURCES/vis-dev.arc-ts.umich.edu.pem` with a file containing your certificate, followed by all CA certificates up to the root certificate, followed by your certificate's RSA public key, all in PEM format.
1. Edit `SPECS/umich-python-websockify.spec` and change the `Source2:` line to reference your certificate file's name rather than `vis-dev.arc-ts.umich.edu.pem`.
1. Run `rpmbuild -ba rpmbuild/SPECS/umich-python-websockify.spec`

## Support

Please send any questions, requests, or feedback to flux-viz-staff@umich.edu.

